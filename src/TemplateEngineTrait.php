<?php

namespace Lliure\Core\Tools;

use Helpers\HtmlHelper;
use Helpers\HttpHelper;
use Twig\Loader\ArrayLoader;
use Twig\Loader\ChainLoader;
use Twig\Loader\FilesystemLoader;
use Twig\TwigFunction;

trait TemplateEngineTrait
{

	private static ChainLoader $loader;

	private static function loader(): ChainLoader{
		return self::$loader = self::$loader ?? new ChainLoader;
	}

	static function setPaths($paths, string $namespace = FilesystemLoader::MAIN_NAMESPACE): void{
		$loader = new FilesystemLoader;
		$loader->setPaths($paths, $namespace);
		self::loader()->addLoader($loader);
	}

	static function addPath(string $path, string $namespace = FilesystemLoader::MAIN_NAMESPACE): void{
		$loader = new FilesystemLoader;
		$loader->addPath($path, $namespace);
		self::loader()->addLoader($loader);
	}

	static function setTemplates(array $templates = []): void{
		$loader = new ArrayLoader($templates);
		self::loader()->addLoader($loader);
	}

	static function setTemplate(string $name, string $template): void{
		$loader = new ArrayLoader;
		$loader->setTemplate($name, $template);
		self::loader()->addLoader($loader);
	}

	protected static function environment(): Environment{
		$twig = new Environment(self::loader());
		$twig->setEngine(static::class);

		$twig->addGlobal('HtmlHelper', new ImportToTwig(HtmlHelper::class));
		$twig->addGlobal('HttpHelper', new ImportToTwig(HttpHelper::class));

		$twig->addFunction(new TwigFunction('url', function ($route_name, $route_parameters = []){
			global $mainRouter;

			try{
				return $mainRouter->getNamedRoute($route_name)->getPath($route_parameters);
			}catch(\Exception $exception){
				return $exception->getMessage();
			}
		}));

		$twig->addFunction(new TwigFunction('asset', function ($path, $query = []){
			return HttpHelper::pathToUri($path, $query);
		}));

		return $twig;
	}
	
	abstract public static function engine(): Environment;
	
}