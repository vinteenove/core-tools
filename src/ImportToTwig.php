<?php

namespace Lliure\Core\Tools;

use BadMethodCallException;

class ImportToTwig
{
	private string $class;

	public function __construct(string $class){
		$this->class = $class;
	}

	public function __call($name, $arguments){
		if(!method_exists($this->class, $name)){
			throw new BadMethodCallException('undefined method ' . $name . 'of '. $this->class);
		}

		return call_user_func([$this->class, $name], ...$arguments);
	}
	
	public function __set($name, $value){
		return $this->class::$$name = $value;
	}
	
	public function __get($name){
		return $this->class::$$name;
	}
	
	public function __isset($name){
		return isset($this->class::$$name);
	}

	public function __unset($name) {}

	public function __invoke(...$parans){
		return (new $this->class(...$parans));
	}
	
}