<?php

namespace Lliure\Core\Tools;

use Twig\Loader\FilesystemLoader;

interface TemplateEngineInterfece
{

	static function setPaths($paths, string $namespace = FilesystemLoader::MAIN_NAMESPACE): void;

	static function addPath(string $path, string $namespace = FilesystemLoader::MAIN_NAMESPACE): void;

	static function setTemplates(array $templates = []): void;

	static function setTemplate(string $name, string $template): void;

	public static function engine(): Environment;

}