<?php

namespace Lliure\Core\Tools;

use Psr\Http\Message\ResponseInterface;

class Environment extends \Twig\Environment
{
	
	/**
	 * @var string<TemplateEngineInterfece>
	 */
	protected string $engine;
	
	/**
	 * @var static[]
	 */
	static protected array $renders = [];
	
	private $name;
	private array $context;
	
	public function render($name, array $context = []): string{
		$this->name = $name;
		$this->context = $context;
		
		$chave = hash('sha256', json_encode($this));
		self::$renders[$chave] = $this;
		return ($chave);
	}
	
	public function html(): string{
		return parent::render($this->name, $this->context);
	}
	
	public static function find(ResponseInterface $response): static|bool{
		
		$strimer = $response->getBody();
		$chave = (string) $strimer;
		
		if(!isset(self::$renders[$chave])){
			return false;
		}
		
		return self::$renders[$chave];
	}
	
	/**
	 * @return string<TemplateEngineInterfece>
	 */
	public function getEngine(): string{
		return $this->engine;
	}
	
	/**
	 * @param string<TemplateEngineInterfece> $engine
	 */
	public function setEngine(string $engine): void{
		
		if(!(in_array('Lliure\LliurePanel\Tools\TemplateEngineInterfece', class_implements($engine)))){
			throw new \Exception('Parameter engine needs to implement TemplateEngineInterface interface');
		}
		
		$this->engine = $engine;
	}
	
}